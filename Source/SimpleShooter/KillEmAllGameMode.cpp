// Fill out your copyright notice in the Description page of Project Settings.


#include "KillEmAllGameMode.h"
#include "EngineUtils.h"
#include "GameFramework/Controller.h"
#include "ShooterAIController.h"


void AKillEmAllGameMode::PawnKilled(APawn* PawnKilled) 
{
    Super::PawnKilled(PawnKilled);
    // UE_LOG(LogTemp, Warning, TEXT("%s died"), *PawnKilled->GetName());
    
    APlayerController* PlayerController = Cast<APlayerController>(PawnKilled->GetController());
    if (PlayerController) // Player died
    {
        EndGame(false);
        return;
    }
    
    for (AShooterAIController* AIController : TActorRange<AShooterAIController>(GetWorld()))
    {
        // If one of the AIControllers isn't dead we quit PawnKilled() without calling EndGame()
        if (!AIController->IsDead()) 
            return;
    }
    EndGame(true);
}

// Calls GameHasEnded on all controllers with the right boolean value depending on who won
void AKillEmAllGameMode::EndGame(bool bIsPlayerWinner) 
{
    // bIsPlayerWinner true -> bIsWinner true for player and false for AI
    // bIsPlayerWinner false -> bIsWinner false for player and true for AI
    for(AController* Controller : TActorRange<AController>(GetWorld()))
    {
        bool bIsWinner = Controller->IsPlayerController() == bIsPlayerWinner;
        Controller->GameHasEnded(Controller->GetPawn(), bIsWinner);
    }
}
