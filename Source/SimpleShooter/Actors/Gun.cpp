// Fill out your copyright notice in the Description page of Project Settings.


#include "Gun.h"
#include "Components/SceneComponent.h"
#include "Engine/EngineTypes.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

// Sets default values
AGun::AGun()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	GunRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	SetRootComponent(GunRoot);

	// The mesh isn't our root component because we want to be able to change the position of the
	// mesh relative to the root of the gun. This way we can place the handle in different
	// locations for different meshes if we have different guns. 
	GunMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Gun Mesh"));
	GunMesh->SetupAttachment(GunRoot);

}

void AGun::PullTrigger() 
{
	if (!GunMesh)
		return;

	if (GunMuzzleFlash)
		UGameplayStatics::SpawnEmitterAttached(GunMuzzleFlash, GunMesh, TEXT("MuzzleFlashSocket"));
	if (MuzzleSound)
		UGameplayStatics::SpawnSoundAttached(MuzzleSound, GunMesh, TEXT("MuzzleFlashSocket"));

	AController* OwnerController = GetOwnerController();
	if (!OwnerController)
		return;
	FHitResult Hit;
	FVector ShotDirection;
	bool bHitSomething = GunTrace(Hit, ShotDirection, OwnerController);
	if (!bHitSomething)
		return;

	// 1) Play particle effect
	if (ImpactFlash)
		UGameplayStatics::SpawnEmitterAtLocation(
			GetWorld(), 
			ImpactFlash, 
			Hit.Location, 
			ShotDirection.Rotation()
		);
	
	// 2) Play sound effect
	if (ImpactSound)
		UGameplayStatics::PlaySoundAtLocation(
			GetWorld(),
			ImpactSound,
			Hit.Location, 
			ShotDirection.Rotation() // Useless in our case, our sounds are the same in every dir
		);

	// 3) Damage the hit actor
	AActor* HitActor = Hit.GetActor();
	if (HitActor)
	{
		FPointDamageEvent BulletDamageEvent(Damage, Hit, ShotDirection, nullptr);
		// - The damage is dealt to the HitActor
		// - The damage causer is this Gun
		// - The event was instigated by the controller of the shooter character owning this Gun instance
		HitActor->TakeDamage(Damage, BulletDamageEvent, OwnerController, this);
	}
}

// Called when the game starts or when spawned
void AGun::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AGun::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

bool AGun::GunTrace(FHitResult& Hit, FVector& ShotDirection, AController* const OwnerController) 
{
	// Getting the player camera viewpoint (i.e. location and rotation)
	FVector PlayerCameraLocation;
	FRotator PlayerCameraRotation;
	OwnerController->GetPlayerViewPoint(PlayerCameraLocation, PlayerCameraRotation);
	// We want the sparks to come out towards the player
	ShotDirection = -PlayerCameraRotation.Vector();

	// End: Start + vector along direction player is facing whose lenght is the weapon range
	FVector LineTraceEnd = PlayerCameraLocation + PlayerCameraRotation.Vector() * MaxRange;
	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this); // Ignoring the gun
	Params.AddIgnoredActor(GetOwner()); // Ignoring the ShooterCharacter that owns this gun
	
	return GetWorld()->LineTraceSingleByChannel(
		Hit, 
		PlayerCameraLocation, 
		LineTraceEnd, 
		// Value the engine assigned to our custom channel
		ECollisionChannel::ECC_GameTraceChannel1, 
		Params
	);
}

AController* AGun::GetOwnerController() const
{
	// GetOwner returns an actor but only pawns have controllers
	APawn* OwnerPawn = Cast<APawn>(GetOwner());
	if (!OwnerPawn)
		return nullptr;
	return OwnerPawn->GetController();
}

