 // Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Gun.generated.h"

UCLASS()
class SIMPLESHOOTER_API AGun : public AActor
{
	GENERATED_BODY()
private:
	// #### COMPONENTS ####

	// VisibleAnywhere because we want to be able to see and edit the properties of the ptrs
	// This will act as our RootComponent
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	USceneComponent* GunRoot = nullptr;

	// The gun mesh in the assets is a skeletal one, so that we can use sockets to spawn emitters
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	USkeletalMeshComponent* GunMesh = nullptr;

	// #### PARTICLE EFFECTS ####
	// These are needed for SpawnEmitter functions in PullTrigger()
	UPROPERTY(EditAnywhere, Category = "Particle Effects")
	UParticleSystem* GunMuzzleFlash = nullptr;

	UPROPERTY(EditAnywhere, Category = "Particle Effects")
	UParticleSystem* ImpactFlash = nullptr;

	// #### SOUND EFFECTS ####
	UPROPERTY(EditAnywhere, Category = "Sound Effects")
	USoundBase* MuzzleSound;

	UPROPERTY(EditAnywhere, Category = "Sound Effects")
	USoundBase* ImpactSound;

	// #### GUN STATS ####
	UPROPERTY(EditAnywhere, Category = "Gun Stats")
	float MaxRange = 1000.f;

	UPROPERTY(EditAnywhere, Category = "Gun Stats")
	float Damage = 100.f;

public:	
	// Sets default values for this actor's properties
	AGun();

	// Function to pull the trigger on the gun from our ShooterCharacter or
	// from the enemies
	void PullTrigger();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	bool GunTrace(FHitResult& Hit, FVector& ShotDirection, AController* const OwnerController);
	AController* GetOwnerController() const;
};
