// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_Shoot.h"
#include "AIController.h"
#include "ShooterCharacter.h"


UBTTask_Shoot::UBTTask_Shoot() 
{
    NodeName = TEXT("Shoot");
}

EBTNodeResult::Type UBTTask_Shoot::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) 
{
    Super::ExecuteTask(OwnerComp, NodeMemory);

    AAIController* OwnerAI = OwnerComp.GetAIOwner();
    if (!OwnerAI)
        return EBTNodeResult::Failed;

    AShooterCharacter* OwnerCharacter = 
        Cast<AShooterCharacter>(OwnerAI->GetPawn());
    if(!OwnerCharacter) // Did the cast succeed?
        return EBTNodeResult::Failed;

    OwnerCharacter->Shoot();
    // No continuous shooting, no need for EBTNodeResult::InProgress
    return EBTNodeResult::Succeeded; 
}
