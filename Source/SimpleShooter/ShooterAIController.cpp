// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterAIController.h"
#include "ShooterCharacter.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"
#include "ShooterCharacter.h"


void AShooterAIController::BeginPlay() 
{
    Super::BeginPlay();

    if (AIBehavior)
    {
        RunBehaviorTree(AIBehavior);

        APawn* ControlledPawn = GetPawn();
        if (ControlledPawn)
            GetBlackboardComponent()->SetValueAsVector(
                TEXT("StartLocation"), 
                ControlledPawn->GetActorLocation()
            );
    }
}

void AShooterAIController::Tick(float DeltaTime) 
{
    Super::Tick(DeltaTime);
}

bool AShooterAIController::IsDead() const
{
    AShooterCharacter* ControlledCharacter = Cast<AShooterCharacter>(GetPawn());
    if (ControlledCharacter)
        return ControlledCharacter->IsDead();
    
    // The check above could fail because AIController detaches from pawn when it dies
    // see AShooterCharacter::TakeDamage()
    return true;
}
