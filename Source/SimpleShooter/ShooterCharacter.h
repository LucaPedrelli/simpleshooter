// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ShooterCharacter.generated.h"

class AGun;

UCLASS()
class SIMPLESHOOTER_API AShooterCharacter : public ACharacter
{
	GENERATED_BODY()

	
public:
	// Sets default values for this character's properties
	AShooterCharacter();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable)
	bool IsDead() const;

	UFUNCTION(BlueprintCallable)
	float GetHealthPercent() const;

	// Overridden from parent Actor.h
	float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
	
	// This has to be public because we need to call it from BTTask_Shoot.cpp
	void Shoot();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	// Functions bound to input
	void MoveForward(float AxisValue);
	void MoveRight(float AxisValue);
	void LookUpRate(float AxisValue);
	void LookRightRate(float AxisValue);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Rotation Rate", meta = (AllowPrivateAccess = "true"))
	float RotationRate = 10.f; // Basically the speed we have when we rotate.

	// To configure which BP class we'll be spawning from ShooterCharacter
	// By using AGun in <> we're restricting the number of class we can
	// choose from. This is just the class, not the actual gun.
	UPROPERTY(EditDefaultsOnly, Category = "Gun")
	TSubclassOf<AGun> GunClass;

	UPROPERTY() // This stores the actual gun .
	AGun* Gun = nullptr;

	UPROPERTY(EditAnywhere, Category="Character stats")
	float MaxHealth = 1000.f;

	UPROPERTY(VisibleAnywhere, Category="Character stats")
	float Health;
};
