// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterPlayerController.h"
#include "Blueprint/UserWidget.h"
#include "TimerManager.h"


void AShooterPlayerController::BeginPlay() 
{
    Super::BeginPlay();
    HUD = CreateWidget(this, HUDClass);
    if (HUD)
        HUD->AddToViewport();
}

void AShooterPlayerController::GameHasEnded(class AActor* EndGameFocus, bool bIsWinner) 
{
    Super::GameHasEnded(EndGameFocus, bIsWinner);

    HUD->RemoveFromViewport();

    UUserWidget* EndgameScreen = nullptr;
    if (bIsWinner)
        // 1st param is the owner. Only certain classes can own widgets, and
        // APlayerController is one of them.
        // The 2nd param is set from the editor and it's our WBP_VictoryScreen class
        EndgameScreen = CreateWidget(this, VictoryScreenClass);
    else
        EndgameScreen = CreateWidget(this, LoseScreenClass);

    if (EndgameScreen)
        EndgameScreen->AddToViewport();

    GetWorldTimerManager().SetTimer(
        RestartTimer, // FTimerHandler
        this, 
        // Function that will be called once the countdown is over (delegate)
        &APlayerController::RestartLevel,
        RestartDelay // Countdown duration
    );
}
