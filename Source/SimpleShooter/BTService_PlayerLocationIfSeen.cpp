// Fill out your copyright notice in the Description page of Project Settings.


#include "BTService_PlayerLocationIfSeen.h"
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"

void UBTService_PlayerLocationIfSeen::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) 
{
    Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

    APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
    if (!PlayerPawn)
        return;
    
    // AIController that owns the UBehaviourTreeComponent.
    AAIController* OwnerController = OwnerComp.GetAIOwner();
    if (!OwnerController)
        return;
    
    if(OwnerController->LineOfSightTo(PlayerPawn))
    {
        OwnerComp.GetBlackboardComponent()->SetValueAsObject(
            GetSelectedBlackboardKey(), 
            PlayerPawn
        );
    }
    else
    {
        OwnerComp.GetBlackboardComponent()->ClearValue(
            GetSelectedBlackboardKey()
        );
    }
}

UBTService_PlayerLocationIfSeen::UBTService_PlayerLocationIfSeen() 
{
    NodeName = TEXT("Update Player Location?");
}
