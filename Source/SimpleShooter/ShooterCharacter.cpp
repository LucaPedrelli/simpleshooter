// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterCharacter.h"
#include "Components/CapsuleComponent.h"
#include "SimpleShooter/Actors/Gun.h"
#include "SimpleShooterGameModeBase.h"

// Sets default values
AShooterCharacter::AShooterCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AShooterCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	// We need to give the method an argument because we don't want to spawn
	// the C++ class but its BP child instead! GunClass will be set in BP
	Gun = GetWorld()->SpawnActor<AGun>(GunClass);

	// Hiding the rifle that is already in the ShooterCharacter's mesh.
	// The name of the bone can be read from the skeleton.
	GetMesh()->HideBoneByName(TEXT("weapon_r"), EPhysBodyOp::PBO_None);

	// Putting the rifle actor in the hand of the skeletal mesh
	Gun->AttachToComponent(
		GetMesh(), 
		FAttachmentTransformRules::KeepRelativeTransform, 
		TEXT("WeaponSocket")
	);

	// The ShooterCharacter is the gun owner. This comes into play for damage
	Gun->SetOwner(this);

	Health = MaxHealth;
}

// Called every frame
void AShooterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// BindAxis: continuous; BindAction: discrete.

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AShooterCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AShooterCharacter::MoveRight);

	// For LookUp we don't need a user-made function, that would just call AddControllerPitchInput, why don't we just 
	// bind to that directly?
    PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &APawn::AddControllerPitchInput);
    PlayerInputComponent->BindAxis(TEXT("LookRight"), this, &APawn::AddControllerYawInput);

	// Controllers need their own axis for movement, otherwise we would have a framerate-dependent game
	PlayerInputComponent->BindAxis(TEXT("LookUpRate"), this, &AShooterCharacter::LookUpRate);
	PlayerInputComponent->BindAxis(TEXT("LookRightRate"), this, &AShooterCharacter::LookRightRate);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Shoot", IE_Pressed, this, &AShooterCharacter::Shoot);
}

bool AShooterCharacter::IsDead() const
{
	return Health <= 0;
}

float AShooterCharacter::GetHealthPercent() const
{
	return Health / MaxHealth;
}


void AShooterCharacter::MoveForward(float AxisValue)
{
	// AddMovementInput is a Pawn function. 
	// GetActorForwardVector gets the forward vector of lenght 1.0 from this Actor, in world space.
	// AxisValue can be positive or negative, determining wheter we move forward or backward.
	AddMovementInput(GetActorForwardVector() * AxisValue);
}

void AShooterCharacter::MoveRight(float AxisValue) 
{
	AddMovementInput(GetActorRightVector() * AxisValue);
}

void AShooterCharacter::LookUpRate(float AxisValue) 
{
	AddControllerPitchInput(AxisValue * RotationRate * GetWorld()->GetDeltaSeconds());
}

void AShooterCharacter::LookRightRate(float AxisValue) 
{
	AddControllerYawInput(AxisValue * RotationRate * GetWorld()->GetDeltaSeconds());
}

void AShooterCharacter::Shoot() 
{
	if(Gun)
		Gun->PullTrigger();
}

float AShooterCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) 
{
	// It's common for overriden methods to call their parent version, so that we don't completely
	// override what's being done in the upper classes (which might have some implementation in
	// there that needs to run).
	float DmgToApply = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	// It doesn't make sense for Health to be negative
	DmgToApply = Health > DmgToApply ? DmgToApply : Health;
	Health -= DmgToApply;

	if (IsDead()) // Death cleanup
	{
		ASimpleShooterGameModeBase* GameMode = GetWorld()->
			GetAuthGameMode<ASimpleShooterGameModeBase>();
		if(GameMode) // Warning our GameMode that this has been killed
			GameMode->PawnKilled(this);
		
		// By detaching the controller the character can no longer move
		// around nor shot. 
		// This has to be done after PawnKilled, because that function needs
		// the controller!
		DetachFromControllerPendingDestroy();
		// Disabling the capsule component we disable collisions with the dead character.
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}

	return DmgToApply;
}
