# Simple Shooter notes

Project plan:
1. Player movement
2. Animation
3. Shooting
4. Health / Death for both us and the AI
5. Enemy AI (behaviour trees)
6. Win/Lose condition

## 166 - Pawns vs Characters in C++

Pawn: anything you can posses (controllable by the player).

Character: inherits from Pawn, it's a pawn that is more like a person. Additions:
- Movement (jump, run, crawl, duck, ...)
- Nav Mesh Movement, which we'll see more when we get to the A.I. section of this section. Basically it's something that helps us move the character around, avoid obstacles and stuff like that.

From blueprints you can see from the components window that pawns have just a DefaultSceneRoot component, whereas characters have many components by default:
- CapsuleComponent (for rough physics and collisions)
- ArrowComponent (tells which way is forward for this character)
- Mesh
- CharacterMovement (what is responsible for giving our object character-like movement)

New GameMode, BP_ShooterCharater as DefaultPawnClass, then change map gamemode to the one we just created. Place a PlayerStart onto it.

## 167 - Character Movement Functions

Many movement functions are already provided to us by the character class:
- `AddMovementInput()`: Takes a vector and tells which direction the character is gonna move. The size of the vector determines how fast you move in that direction.

- `AddControllerPitchInput()`: This is about looking up and down
- `AddControllerYawInput()`: Left and right
- `Jump()`

what we want to go ahead and do is create some bindings in our character actor class that's bind some input. Input must be bound in the `SetupPlayerInputComponent` function.

## 168 - Controller Aiming

We're gonna add controller movement, (PS4, XBoxOne, ...).

We first add the controller bindings for movement, which is really easy and they are linked to the left thumbstick. So you might think: we just map the camera to the right thumbstick! No, that's not ok. Why?

See minute 2:20. Basically: while mouse movement per frame automatically scales with FPS, controller movement per frame does not, making the **game frame dependent**, which is very bad because that would mean that on faster computers the game is easy. -> we need to write a rotation rate variable to make our rotation frame independent.

1. New axis: "LookUpRate".
2. New function called by the binding. There you call `AddControllerPitchInput(AxisValue * RotationRate * GetWorld()->GetDeltaSeconds())` instead of just having `AxisValue` as argument. RotationRate isn't that important, it is just a settable speed. GetDeltaSeconds is the really important part, it is what makes our LookUp speed frame-independent.

Why don't we do the same with the movement binding? Because `AddMovementInput` is a function that already assumes that its argument you pass to it is a ratio and not the actual amount to move, it will automatically do this computation for you basically, while `AddControllerPitchInput` doesn't, it assumes that what you're passing it is exactly how much you want to move!

## 169 - Third Person Camera Spring Arm

We want a 3rd person shooter. Simply adding a camera to your character isn't going to be enough: the camera will be able to go through walls, causing the player to momentarely loose a correct view of the character. Besides, we currently have a camera rotation problem: moving the mouse doesn't seem to be affecting our camera.

To fix the camera rotation problem simply go to Camera Options -> Use pawn control rotation. Now the mouse affects the camera, but it rotates around its center, not around the character... 

To affect the wall glitch and the camera rotation problem we need another component: the SpringArm! Make camera a child of the springarm, then disable "use pawn control rotation" for the camera and enable it for the springarm.

## 170 - Skeletal Animations

Let's talk about animations and how we can use the same animation on different type of meshes using something called a skeleton.

Green animations -> basic animation sequences.

Skeletal mesh animation: mesh that has a skeleton, i.e. that can be manipolated through a bunch of bones.

The skeleton kind of exists within the modelling program that was creating this mesh, but a skeleton Unreal has a slightly different function: its main function is to tie together the meshes and animations.

**You can have multiple meshes which all use the same skeleton asset in Unreal and set multiple animations that use the same skeleton**.

The skeleton brings everything togheter basically.

BP_ShooterCharater->Mesh->Animation Mode->Use animation asset->select a basic anim seq.

If you change the skeletal mesh to one that has a different skeleton the animation won't work: you need the same skeleton.

## 171 - Editing Collision Meshes

Fixing weird glitch to the right of the first spawn point. Something is wrong with our collisions!

Player collision -> hey, there is an error in our asset pack! :flushed:

## 172 - Animation Blueprints

We're gonna be looking at how we can use the anim graph of an animation blueprint to blend animations together using a series of variables.

The main point of the animation blueprints is to bring together all the animation logic and allow us to kind of blend and merge and switch between animations such that our game logic can then drive that animation blueprint.

Animation graph (Animgraph): like blueprints graph, except we have no execution node. All we've got is the data that flows through the graph.
  
And what does flow through the graph? Basically, we're trying to get to an output pose, we should model our character into it. We do that by connecting up lots of different nodes to this result node and flowing through that we end up getting the data.

Blend node: 2 input poses, 1 output pose. Let's blend "jog_forward" and "jog_backward" togheter! Alpha value blends between the two.

Blend are not the best way to do this, we're soon gonna learn about something called "Blend space" which automatically allows us to blend movement animations way nicely.

## 174 - 2D Blend Spaces

Blend spaces allow us to smoothly move between different animations (better for locomotive animation like we did in the previous lesson).

We have 2 dimensions: forward-backward dim, and the left-right dim -> we need a 2d blend space!

An animation for each side and we can move the cursor to blend between the 4 animations! Problem: in the middle the animation is strange, we don't have an idle animation.

To have better angular animation, let's change the dimensions to angle and speed!

Hor: angle [-180, 180]

Ver: speed [0, 100]

speed = 0 -> idle animation, always
speed = 100 -> different animation depending on the angle!

## 175 - Connecting Animation To Gameplay

We prepared our animation, now we need to link it to the gameplay! How do you do that? Well, animation blueprints have an event graph like other BPs.

BP_ShooterCharater -> Mesh -> Animation mode -> Animation BP. Then select your animation in Animation Class.

Now BP_ShooterCharater is the pawn owner of that animation.

Event graph, update function: we want to grab the information about the pawn's movement (speed + angle).

Get pawn owner -> get velocity -> vector lenght -> set speed on event BP update animation.

That's it for the speed, in the following lecture we'll connect the angle to the gameplay.

#### Relation between gameplay and animations

Why don't we just have the animations, drive our gameplay? You don't want your animations to constrain the design and game feel, you want the animations to come off the back of your game feel and make it look good.

You want to be able to change up the game first to get there rather than have to go and try and record a different running animation or find a different running animation and then realize that actually that didn't improve the feel of the game.

That's why you want to keep these two things as separate as possible. There are times when things really must line up with the animations, but 90% of the times that's not the case.

## 176 - Inverse Transforming Vectors

We need to get the angle local to the pawn from the global angle.

Local space: the character is the center of the world, he doesn't move, the world around it does instead.

We're interest to a local angle relative to where the player is looking.

Moving to the right locally means that if the player is turned around of 180 degrees we shouldn't play a left movement animation just becuse he turned around!!

Position vector: tells us where something is

Direction vector: tells us the direction of something. Example of direction vectors: velocity, acceleration. The velocity vector doesn't tell you "the velocity is there in space", it tells you along which direction you're moving and its lenght determines how fast you're moving.

Local to global: direct transformation.

Global to local: inverse transformation. We're interested to this case.

Position and Direction vectors transform differently (see figure in this lesson), therefore we have 2 function: inverse transform direction (applies rotation and scale only) and inverse transform location (will move the position aroun). We're interested in the former of course.

From pawn owner use `get transform actor` and, from `get velocity` (see previous lesson), use `inverse transform direction`, that will get the transform as input as well as the direction vector. The output vector can be transformed into the 3 angles via `rotation from x vector` if you split the output pin (rotator) in its components. 

## 177 - Calculating Animation Speeds

Let's try and fix the foot sliding issue. 

Blend space => we arbitrarely set a speed of 100 cm/s => If our animation is faster than 100 cm/s we get foot sliding. Of course this means our animation "limits" our gameplay.

How do we calculate the speed of an animation so that we can set it as limit? It is not told us automatically by Unreal!

Speed of animation: we want to know how fast the foot moves from the point it starts touching the ground to the point it's starting to leave the floor => track speed of ankle bone (it is called foot_r, it's in world reference frame)!

speed = (yf - yi) / (tf - ti). Why the y-axis? Because the feet move along this axis for this animation.

Finally, you use these values instead of the old ones in the blend space.

## 178 - Gun Actors

Gun architecture: how to make them swappable & how to create different kinds of guns.

You don't want to use the gun that's already in the animation we currently have, because you often want to make guns swappable or, at least, you want different characters to have different guns. Solution? Architect the gun as a separate thing to the character.

You might say:"okay, well, let's create a gun component and we can stick that gun component underneath our mesh". That would work, except that then we lose the modularity that allows us to add multiple components to the gun.

So what we really want to do is go down the route of **having an actor for the gun that is childed to the shooter character**.

The shooter character can have multiple components which are child to each other: the capsule, and the mesh as a child of the capsule. The components of the gun can be childed to that: the mesh gets childed to the mesh of the character, a particle effect might be childed to the gun's mesh etc. See 1:47.

There is no hierarchy of actors: there only is a hierarchy of scene components! There are actors that own scene components, and so they end up having a hierarchy, but they don't actually have a relationship among themselves.

#### C++ Gun Actor

We want a scene component as root component and then a mesh as a child of that. This is because **we want to be able to change the position of the mesh relative to the root of the gun** so that we can place the handle in different locations for different meshes if we have different guns.

For some reason the guns are skeletal mesh instead of static mesh... therefore, we need to use those `USkeletalMeshComponent`!

We can now create different BP classes from this C++ Gun class: depending on BP subclass we can have different kinds of guns!

## 179 - Spawning Actors At Runtime

We're gonna spawn our gun actor at runtime from our character class in C++ (spawn an actor from within another actor).

Add these to ShooterCharacter.h:
```cpp
// To configure which BP class we'll be spawning from ShooterCharacter
// By using AGun in <> we're restricting the number of class we can
// choose from. This is just the class, not the actual gun.
UPROPERTY(EditDefaultsOnly, Category = "Gun")
TSubclassOf<AGun> GunClass;

UPROPERTY() // This stores the actual gun.
AGun* Gun = nullptr;
```
And in BeginPlay:
```cpp
Super::BeginPlay();

// We need to give the method an argument because we don't want to spawn
// the C++ class but its BP child instead! GunClass will be set in BP
Gun = GetWorld()->SpawnActor<AGun>(GunClass);
```
Then compile, go to the BP_ShooterCharacter blueprint and set BP_Rifle as GunClas.


At the moment the gun gets spawned in the middle of nowhere and it's not correctly childed; that's gonna be the subject of the next lecture.

## 180 - Attaching To Meshes Via Sockets

How can we attach the mesh of the BP_Rifle to the hand of the skeletal mesh of the ShooterCharacter?

Let's begin by removing the gun that's currently in the ShooterCharacter hand => hide the bone that has the gun mesh via c++ at BeginPlay():
```cpp
// Hiding the rifle that is already in the ShooterCharacter's mesh
GetMesh()->HideBoneByName(TEXT("weapon_r"), EPhysBodyOp::PBO_None);
```
Ok, now we need to add a new socket (pointer to which you can plug in other things, like the gun) to the skeleton so that we can put a new gun. The gun will be childed not to the mesh as a whole but to a specific bone of the mesh, which means that it will move correctly during the movement animation of the character. See how this is done at 4:30.

Now, attachment. At runtime we don't want to do a SetupAttachment, as we would do in the constructor.
```cpp
Gun->AttachToComponent(
		GetMesh(), 
		FAttachmentTransformRules::KeepRelativeTransform, 
		TEXT("WeaponSocket")
);
```
The final point is going to be that we want to set up the gun to have the ShooterCharacter class as its owner. This is a subtle point, because we said that there isn't any hierarchy between actors. Well, there isn't in the sense of transforms, but actors do have ownership. This ownership really comes into play in multiplayer and in damage. Also, it allows the gun to know who is the owning character. All of this will be useful in the future, don't worry about it for now, let's just setup the owner via:
```cpp
// The ShooterCharacter is the gun owner. This comes into play for damage
Gun->SetOwner(this);
```

## 181 - Shooting Architecture

Our gun doesn't shoot any bullet yet! First step: print a log message when the left mouse button is pressed.

Why the gun as core to our shooting architecture? To support various methods of shooting (a rpg shoots differently from a rifle for example).

BindAction to left mouse button. You can't bind on the gun actor class directly, you need an intermediate function in your shooter character which calls a public function on its gun reference. This public gun function is what allows us to fire!

## 182 - Spawning Particle Effects

We want to spawn a particle effect when we press the trigger (LMB).

SpawnEmitterAtLocation or SpawnEmitterAttached? In our case the 2nd function is better since we want the emitter to be attached to the gun!

Why was the rifle made as a skeletal mesh by the UE team? Because this way we can use SpawnEmitterAttached and use sockets!

We're gonna need many arguments to use SpawnEmitterAttached, so let's add them to our gun and make them editable from BluePrints!

Different particle effects can mean different durations! For example, the first one I tried lasted forever once the trigger was pulled, but that is not the kind of weapon we want right now: we want to shoot only once when the trigger is pulled. (see last part of lesson to see what I mean).

## 183 - Player View Point

First steps towards ray tracing.

Very important for aim: **the camera and the gun not necessarely point in the same direction! If their directions are different, the player is gonna be confused** because he thinks he is pointing the gun in direction A (camera's direction) while the gun is shooting along direction B!

You should ray cast from the camera, not from the gun, in 3rd person shooters!

We need the controller's viewpoint, that requires us to go to the character from the gun, find the character's controller and then find its viewport location and rotation.

What we're gonna do in this lecture is try to visualize and confirm that that viewport location rotation is what we would expect. We're going to do that by drawing some debugging cameras when `PullTrigger()` is called via `DrawDebugCamera()`

#### Test 1: using actor location and rotation

```cpp
#include "DrawDebugHelpers.h"
// ...
DrawDebugCamera(GetWorld(), GetActorLocation(), GetActorRotation(), 90.f, 2.f, FColor::Red, true);
```
The little cameras are spawned at the rifle location (the socket where it is attached to the ShooterCharacter) -> not good!

#### Test 2: using controller viewpoint location and rotation

We first get the controller, then we get its viewpoint through `GetPlayerViewPoint`, which will use the eyes of the pawn for an AI controller the player's camera for a human player.

```cpp
// Cast because GetOwner returns actor but only pawns have controllers
APawn* OwnerPawn = Cast<APawn>(GetOwner());
if (!OwnerPawn)
	return;
AController* Controller = OwnerPawn->GetController();
if (!Controller)
	return;
FVector PlayerCameraLocation;
FRotator PlayerCameraRotation;
Controller->GetPlayerViewPoint(PlayerCameraLocation, PlayerCameraRotation);

// Drawing the debug camera
DrawDebugCamera(
	GetWorld(),
	PlayerCameraLocation, 
	PlayerCameraRotation, 
	90.f, 
	2.f, 
	FColor::Red, 
	true
);
```

If you spawn both cameras simultaneously, by using both functions above, you can clearly see that they spawn at different location and point in different directions.

## 184 - Line Tracing by Channel

`LineTraceByObjectType` vs `LineTraceByChannel`: what's the difference?

- `LineTraceByObjectType` says:"I want to search through this line and see if I find an object that matches this type."
- `LineTraceByChannel` is better in our case because it allows us to define our own custom channels, i.e. define what sorts of objects are transparent to that channel and which ones block it.

What we want to be able to do is say:"Okay, maybe a Bush type of thing can't can't block a bullet whereas a wall will block a bullet, but it won't be taking any damage" (we will deal with damage later on).

To be precise, the function we're gonna use is `LineTraceSingleByChannel`. To use this function we need to setup a `TraceChannel` first: go to project settings > Engine - Collisions > Trace Channel and create your own trace channel. See the video from 2:50 to see how it's done.

We created our trance channel: how do we use it through code? The TraceChannel is an enum: which of its values our new channel has been assigned to? Go to your project folder > Config > DefaultEngine.ini > ctrl+f to search the name of your channel (I called it Bullet) > see which Channel it has been assigned to (in my case `ECC_GameTraceChannel1`).

What else does `LineTraceSingleByChannel` need? The start and end FVectors! The start is our player camera's location, but what is the end?

Think about it: the end (with respect to the world of course) is the sum of the start (which is also with respect to the world's origin of course) + the vector pointing the direction the player is looking (the lenght of this vector is the weapon's range and we choose that).

`LineTraceSingleByChannel` returns a boolean representing if we hit something or not: on case we did, we draw a point via `DrawDebugPoint` function.

## 185 - Impact Effects

Let's have some effects play when we hit something! Let's use `SpawnEmitterAtLocation` this time. This function is very similar to `SpawnEmitterAttached`, but it requires a location (Hit.Location obviously) and a rotation. The rotation is obtained from the vector pointing at the player along the PlayerCameraLocation direction, like follows:
```cpp
FVector ShotDirection = (-PlayerCameraRotation.Vector()).Rotation();
```

## 186 - Dealing Damage To Actors 

If you bring in a character into the scene, then automatically unreal is going to make this character A.I. controlled (the ones that spawn at the player starts will be player controlled). Let's bring a ShooterCharacter in our map.

**Doing damage is a 2 part process: sending the damage and receiving it**. In this lecture we'll be talking about sending damage to the player.

```cpp
virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser);
```
- float DamageAmount: do I really need to explain this?
- FDamageEvent DamageEvent: to give us more info on the damage that happened
- AController* EventInstigator: controller responsible for dealing this damage
- AActor* DamageCauser: actor responsible (gun, projectile, ...)

Everything is quite clear, except for...

#### FDamageEvent

There are actually two subtypes for this damage event (see by ctrl+cliking on it).

In practice, you're not going to actually create an EF damage event, but rather you'd be creating an FPointDamageEvent (bullets / guns) or you'd be creating an FRadialDamageEvent (granade, has a radius). Those are the two main types of damage that exist in Unreal.

You can create more different types of damage for your own custom stuff, but we're just interested in the ones given by Unreal.

So, we're going to create an FPointDamageEvent using the following constructor:
```cpp
FPointDamageEvent(float InDamage, struct FHitResult const& InHitInfo, FVector const& InShotDirection, TSubclassOf<class UDamageType> InDamageTypeClass)
		: FDamageEvent(InDamageTypeClass), Damage(InDamage), ShotDirection(InShotDirection), HitInfo(InHitInfo)
	{}
```
We already have some of the stuff ready, like InHitResult and InShotDirection.

InDamageTypeClass is a subclass of DamageType, which can be used to pass in more information (fire type damage, piercing type damage, ...) rather than creating a different FDamageEvent type. In our case, because we don't need any kind of damage type, we're going to pass in a null pointer here instead.

#### Back to our PullTrigger method 

Let's create an FPointDamageEvent in case we hit something and then call TakeDamage

## 188 - Overriding TakeDamage

We're currently dealing damage through the Gun actor but we aren't receiving it through the ShooterCharacter!!

One could store damage against a component (like we did in ToonTanks project) or against the actor itself by virtualization! Lesson 187 was a simple theoretical explanation on how virtualization in C++ works. We'll now override the TakeDamage method, which is virtual! Of course, it will be overridden in the class which actually takes the damage, i.e. the ShooterCharacter class.

In overridden methods is often a good idea to call the `Super::` version of the method as first thing, so that any implementation that might be in the higher class isn't lost and we don't completely override it.

Two variables: MaxHealth and Health (i.e. the current health of the character). Health shouldn't be editable from the editor, only MaxHealth.

The damage to apply is the amount of health if health < gun damage. This way we're assured we don't go below 0 health (which wouldn't make much sense).

## 189 - Blending Animations by Booleans

New node Blend Poses By Boolean which allows us to show a death animation for our character.

Animation blueprint > AnimGraph > we want to switch over from playing our blendspace to play our death animation and never go back from there using the new node mentioned above.

Looping must be unchecked from the Death_Forward animation so that it plays only once.

Look at the video for the node attachments in the AnimGraph, however they're all very simple.

## 190 - Blueprint Pure Nodes

We're now going to hook our health system to our AnimGraph

If we go through to our animation event graph and have a look at how this is currently working for properties such as get velocity, get active, transform etc, we're going into the pawn and we're doing a get function on it every time we want to update animation.

So we want to kind of follow along in this model where we're going to go and get whether or not our player is dead. Therefore, we need to expose a function from c++ to the blueprints, a function that tells us if the player is dead based on a helth check. To do so, we'll use `UFUNCTION(BlueprintPure)` for method. Why not `BlueprintCallable`?

A **pure node** in a Blueprint is a node that doesn't have an execution pin (i.e. the triangular white pin that some nodes have). It's usually safe to say that, **in Blueprints, something doesn't have an execution pin when it doesn't have any impact on the thing that it's calling it only on its result**. This is why BlueprintsPure and const keyword often go togheter. BlueprintsPure is actually a bit stronger than const because it states that the node doesn't change anything not only in our ShooterCharacter class, but globally, anywhere.

Non pure nodes currently in our graph:
- SET 
- Event Blueprint Update Animation

The others are all pure.

After you made the function, just cast the output of `Try Get Owner Pawn` node to `ShooterCharacter` and then call the function on it. Create a new variable `IsDead` in the BP where you'll store the value that is output by our c++ function, and finally use it in the AnimGraph.

## 191 - Create And Setup An AI Controller

In this lecture, we're going to be setting up the basics in place for our A.I., which is going to be quite a series of lectures.

So we're starting with a revision of A.I. controllers and blueprints.

The first thing we always need to do with A.I. is to have an A.I. controller: we're gonna go over and create a C++ one, create the blueprint subclass, and hook it up as the default for the pawn.

1. Create the c++ class `ShooterAIController`
2. Create a bp sublcass based on that, `BP_ShooterAIController`
3. Go to `BP_ShooterCharater` and use `BP_ShooterAIController` as AI Controller Class

So we've just gone through a quick revision of how we typically set up A.I. controllers. The rest of the lessons on AI will consist in actually making this new controller do something, basically aiming at our player.

## 192 - AI Aiming

We'll use a built-in functionality in Unreal4 that allows our AI character to face towards the player.

- AIController.h > SetFocalPoint: we can make the AI focus on a particulare point 
- AIController.h > SetFoucs: focus on a particular actor. 

We want the latter because actor positions get updated, points are static. There is also a ClearFocus function that makes the AI stop focus on something. 

All function have a `EAIFocusPriority::Type` enum parameter: the idea is that we have different levels of priority, three: gameplay, move and default. Gameplay is the highest level of priority (highest value), only if gameplay isn't set (because you cleared that level of priority) than the AI is going to look at where it is going (Move level) and, if it's not moving anywhere (we cleared the move level too) then it's going to look at the default level of priority.

Let's start from BeginPlay: it is a protected function, so we need a protected section in our controller class. In it, we want to get hold on the pawn actor with `GetPlayerPawn` and set focus to it.
```cpp
void AShooterAIController::BeginPlay() 
{
    Super::BeginPlay();

    // Get hold of the player pawn
    APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
    // Set focus to the player. Function of AIController parent class.
    SetFocus(PlayerPawn);
}
```

## 193 - Nav Mesh And AI Movement

We're going to be getting at A.I. to follow us and navigate around the world, avoiding obstacles.

When you're moving with A.I., A.I. needs to do something called **pathfinding**: It needs to know which are the obstacles it can't move through and plan its routes to come around the obstacles and come towards us.

The way this typically works, specifically here in Unreal, is that we generate a **nav mesh** which tells us where is walkable within a level and then, using various algorithms (one of which is called a style pathfinding), you can navigate on this mesh and create a route plan. The AI then uses this route plan to move along. Fortunately, a lot of that is done for us and we don't have to code this from scratch.

#### 1. Nav Mesh

First of all, we need to see the nav mesh. Go to windows > placed actors > search for nav mesh > place it into the map, making sure that the floor is inside it. This creates a volume, a kind of box, and everything within that box is going to be checked for how navigable it is, basically, whether it's flat, whether it's not too steep (for example, walls can't be traversed because they're too steep). Enable the visibility of nav meshes in the viewport by going on show > navigation. If we move the nav mesh to where there's an obstacle, you can see that the mesh kind of cuts out around this obstacle.

What we actually want to do is have this volume encompass the whole of our level by changing the brush settings and making it larger.

#### 2. Routing Algorithm

Open the BP_ShooterAIController editor -> there is a `PathFollowingComponent`. That component is responsible for finding the nav mesh and then basically allowing us to create paths and follow them and move along them automatically. We don't have to build anything on our own, we just need to call one of two functions in the AIController called `MoveToLocation` and `MoveToActor`. Of course, in this case we want to use `MoveToActor`. This function has to be called in Tick otherwise, once the AI controlled character has reached us, it will stop trying.

## 194 - Checking AI Line Of Sight

It's not ideal that our enemy A.I. can currently followe us around corners, no matter how far we go, how no matter how much we hide behind pillars.

We're going to be getting a line of sight to our player from the enemy so that we are able as the player to hide behind obstacles and not have that enemy follow us.

AIController.h has a function called `LineOfSightTo` which we'll use to check if the player character is visible from our AI controlled character. 

Structure of Tick:
```cpp
// If line of sight
  // Set focus
  // Move to
// else
  // clear focus
  // stop movement
```

This is done as follows:
```cpp
void AShooterAIController::Tick(float DeltaTime) 
{
    Super::Tick(DeltaTime);

    if(!PlayerPawn)
        return;
    
    // Many of the function that are here are in AIController.h, which is parent to this class.
    // Therefore, we don't need to use AIController::FunctionName()

    // Checking if we have the player character in our line of sight
    if (LineOfSightTo(PlayerPawn))
    {
        // The order of SetFocus and MoveToActor isn't important because SetFocus sets the focus on
        // Move level of priority

        // Set focus to the player on Gameplay level of priority.
        SetFocus(PlayerPawn);
        // Move to the player using navigation data to calculate path. The algorithm is taken care
        // by PathFollowingComponent in BP_ShooterAIController.
        MoveToActor(PlayerPawn, AcceptanceRadius, false);
    }
    else
    {
        // It would be ridiculous for the AI character to keep rotating towards the player if it is
        // hidden behind walls. The focus was set on Gameplay level of priority, so we clear that.
        ClearFocus(EAIFocusPriority::Gameplay);
        // Stop chasing the player character.
        StopMovement();
    }
}
```

## 195 - Behaviour Trees and Blackboards

Behaviour Trees (BT) and Blackboards (BB) are 2 new types of assets that are linked togheter and allow us to make sophisticated AI behaviour in an easy way.

- Behaviour Tree: allows us to create trees of behaviors for our AI, it is a great way of putting together natural behaviors in your games.
- Blackboard: memory of your AI, it's essentially where you store a bunch of variables that you input from the gameplay (very much like we did with the animation system).

The BT reads/writes the BB properties to decide what the AI is going to do. BT and BB are entirely within the editor, they have their own editor tools (they're not blueprints) and we can interface with them from c++. We're gonna be using the editor only for a while.

The AIController is completely responsible for running AI behaviours, and there is no exception here: BT and BB are just another thing the AIController can use, therefore the ShooterAIController must have a way to reference which BT to run.

Let's comment everything we did in the Tick method because we're going to replace all that functionality with BT functionality. Then:
```cpp
// in ShooterAIController.h
UPROPERTY(EditAnywhere)
	UBehaviorTree* AIBehavior;
  
// ShooterAIController.cpp
void AShooterAIController::BeginPlay() 
{
    Super::BeginPlay();

    if (AIBehavior)
        RunBehaviorTree(AIBehavior);
}
```
Finally connect the BT you created to the BP_ShooterAIController and you're set up.

## 196 - Setting Blackboard Keys In C++

We want to be able to set some state from our gameplay in the blackboard for the A.I. to act upon.

We want to set a new key in our BB, the PlayerPawn location. In order to do that, we need to find out how we can gain access to it in C++.

- AIController.h -> GetBlackboardComponent
- BlackboardComponent.h -> SetValue

Here is the code snippet in BeginPlay:
```cpp
void AShooterAIController::BeginPlay() 
{
    Super::BeginPlay();

    if (AIBehavior)
    {
        RunBehaviorTree(AIBehavior);

        APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
        GetBlackboardComponent()->SetValueAsVector(
            TEXT("PlayerLocation"), 
            PlayerPawn->GetActorLocation()
        );
    }
```
Now we just need to create the key on the BB side, making sure that its type and name match the ones we set in C++.

## 197 - Behavior Trees Tasks And Sequences

Via Behavior Trees, we're going to be getting our A.I. to be patrolling back and forth between different locations.

We're going to create a behavior that allows us to go from the place where we're starting to the player's location and then go back to our original starting location.

So there is a distinction in behavior, trees between things called tasks and things called composite nodes (e.g. the Sequence node).

- A task is something that you do
- A sequence is an ordered sequence of such tasks

Tasks run across multiple ticks. For example, the "Move To" task runs every frame until it is completed, i.e. the AI reaches the player.

Let's attach, from the ROOT node, a sequence node. Then, let's attach 4 task nodes: Move To, Wait, Move To, Wait. Because of the sequence node, they'll run in order until one of them fails. Once we're finished, we start from the ROOT node again. The nodes have numbers that indicate the order of execution, starting from 0 (the sequence node itself). 

The Move To node takes in a BB key, and we can pass the PlayerLocation to the 1st one. To the 2nd one we pass the StartLocation, which can be obtained from c++ via GetPawn() (function that returns the controlled pawn).

## 198 - BT Decorators And Selectors

We want to implement the following behavior: when we can see the player, we go and move as close to them as we possibly can. If the player goes out of sight, then we try and move to the last known location of the player to find them and, once we have exhausted that option, maybe we go back to a different location or maybe just stay there for now.

For this behaviour we need to understand how to implement choices in our BTs based on the data we have available. To do so, we need something called a selector node.

Differently from the sequence, if you have a selector the tasks under it will run until one succedes. Basically, it is expecting all of them to fail except one. In fact, it's the opposite of the sequence, which runs all the task until one of them fails.  
Selectors are useful because sometimes we want to choose the first option that is viable.

Root -> selector -> 2 sequences attached to it: chase and investigate.

To make the sequences fail we can add decorators to them. Decorators change the way the sequence node works. We are interested in the blackboard decorator.

We want the PlayerLocation to be set only when the player is visible, and then we're going to add another vector to our BB "LastKnownPlayerLocation".

Decorator to chase seq: If player location is set -> move to PlayerLocation

Chase -> Move To LastKnownPlayerLocation

#### Observer aborts

One important thing to note about these BB conditions is that by default, once we have made the decision to start executing the chase node, we're not going to re-evaluate that. So, if for some reason we lose sight of the player while we're moving to that player's location, we're going to keep trying to get there. THIS IS NOT WHAT WE WANT!!

Click on BB decorator -> flow control -> observer aborts, there are 4 options:
- None: the default we described above
- Self: if the decorator condition suddently becomes false, stop whatever you're doing and re-evaluate your selector above.
- Lower Priority: if the decorator condition suddently becomes true, stop whatever else you were doing and re-evaluate the selector
- Both: self + lower priority. 

In our case, we want "observer aborts both". We just basically want to always go and do whatever path is dictated by this BB condition.

Basically: "observer aborts" stop things in action instead of waiting until the action is finished before re-evaluating it.

#### C++ side 

In ShooterAIController Tick method check line of sight with PlayerPawn: if true GetBlackboardComponent()->SetValueAsVector for PlayerLocation and LastKnownPlayerLocation, else GetBlackboardComponent()->ClearValue(TEXT("PlayerLocation")).

To reduce the enemy speed (which makes debugging a lot easier) select it in the viewport and go to CharacterMovement component -> max walk speed.

## 199 - Custom BT Tasks In C++

It is possible to make entirely custom tasks for BTs in C++!

New functionality to add: once we're done investigating, we move on to going back home.

Add a Move To task after investigation, which just returns us to the start location.

Whate we need now is to have some notion of whether we've already visited the last known play location or not. The way we're going to do that is essentially by unsettling the last known player location once we have investigated it. We need a node to unset a BB key, and we're going to create it via C++ (it could be done with BPs too, but C++ is way more powerful).

We need to create a new C++ class. Which base class are we gonna use? "Move To" uses Blackboard base, and what this means is that it has the Blackboard key dropdown that allows us to choose a BB key to work with, which is exactly what we need! We'll name the new class `BTTask_ClearBlackboardValue`

After a bit, you'll get a linker error. To fix that, go to `SimpleShooter\Source\SimpleShooter\SimpleShooter.Build.cs` -> `PublicDependencyModuleNames` -> add "GameplayTasks" -> recompile.

If we were to add this into our behavior tree at the moment, it would have an ugly name, and we want a nice human readable name for this task. The way we're gonna do this is by adding in a public constructor and setting a property.
```cpp
UBTTask_ClearBlackboardValue::UBTTask_ClearBlackboardValue() 
{
    NodeName = TEXT("Clear Blackboard Value");
}
```
Now you can add your node to the BT but, obviously, it doesn't do anything yet.

## 200 - Executing BTTasks 

We'll make our custom BTTask actually do something.

Let's go to `UBTTaskNode` implementation (which is 2 grades above with respect to our custom UBTTask in the hierarchy, to go there you can use F12 on the parent name 2 times).

The functionalities we probablly want to implement are listed at the top of the header and are:
- ExecuteTask: called when the task starts to execute, the 1st frame
- AbortTask: when it's decided that we need to stop executing, for example, because a condition has become false.
- TickTask: Happens all the time that the task is executing, but not the first tick (see ExecuteTask). That's for behaviors that might run for longer than one tick, for example, moving to an enemy might take multiple ticks.

Since our task is simply just going to clear the blackboard value as soon as it is executed and then then it's going to finish straight away, we just need execute task.
```cpp
virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory);
```

Why NodeMemory argument? There is only one task class instance per all of the BBs, these instances get shared between everything. Therefore, if you want to save some information that's related to a node in one particular BT, then you need to store that information in this `uint8*` memory pointer. But we're not doing anything like that.  
Ultimately, what we're interested in is the fact that we can get to our `OwnerComp`, the behavior tree component, and from there we can get to other things, such as the pawn etc. But that's also the subject of another lecture: now we'll just implement the ExecuteTask method.

Don't forget to `Super::`!
```cpp
EBTNodeResult::Type UBTTask_ClearBlackboardValue::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) 
{
    Super::ExecuteTask(OwnerComp, NodeMemory);
    OwnerComp.GetBlackboardComponent()->ClearValue(GetSelectedBlackboardKey());
    return EBTNodeResult::Succeeded;
}
```
Notes:
- OwnerComp.GetBlackboardComponent() returns us the BB from the BT owner. You don't need to check that OwnerComp != nullptr because, differently from ptrs, references cannot be null!
- GetSelectedBlackboardKey() gives us the key that is selected in the BT editor, in our case it will be "LastPlayerKnownLocation"
- the return type is an enum. If you check its values they're pretty straightforward except for "InProgress" which basically says "keep calling tick until I tell you otherwise, until I say that I have finished". We're not gonna use this, but remember that if you need to create a task that is long running, i.e. that will run across multiple ticks, then we're going to have to return "InProgress".

## 201 - BTTasks That Use The Pawn

We're going to be making an enemy more of a threat by implementing a shoot node in C++.

Changes before the actual shoot node:
- Make 5m of distance acceptable for the Move To node
- At the moment the AI finishes the Move To node completely before reading PlayerLocation and changing where it is directed to. This causes the movement to look like a series of straight lines, and we don't want that. To fix this, you need to check the "check blackboard value" checkbox of the node.

Now, the behavior that we want is that once he comes within radius he's going to stop and shoot at us.

We need a new type of c++ task. We don't actually need to use a key because we're just going to try and shoot, we're gonna pull the trigger on our gun, all the aiming is gonna be done elsewhere in our behaviour tree. So, let's simply use `BTTaskNode` as base class.

Once the class is ready, override `ExecuteTask`. In it, we need to get hold of our pawn so that we can call `Shoot()` on it (which in turn will call `PullTrigger()` on the Gun member of ShooterCharacter).  
To be more specific: we need to get hold of the AIController: once we've got that, we can simply call `GetPawn()` as we discovered time ago.

>In `ExecuteTask` we have a BTComponent as argument, and we know that our AIController has a BTComponent member, so maybe from the BTComponent we can do something? Go to that with F12, search for "controller" on the file -> nothing happens. Mh. But maybe the parent of BTComponent, BrainComponent, contains something? YES!! GetAIOwner()!

The code, with all the safety checks in place, is the following:
```cpp
EBTNodeResult::Type UBTTask_Shoot::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) 
{
    Super::ExecuteTask(OwnerComp, NodeMemory);

    AAIController* OwnerAI = OwnerComp.GetAIOwner();
    if (!OwnerAI)
        return EBTNodeResult::Failed;

    AShooterCharacter* OwnerCharacter = 
        Cast<AShooterCharacter>(OwnerAI->GetPawn());
    if(!OwnerCharacter) // Did the cast succeed?
        return EBTNodeResult::Failed;

    OwnerCharacter->Shoot();
    // No continuous shooting, no need for EBTNodeResult::InProgress
    return EBTNodeResult::Succeeded; 
}
```
Make your BT: inside "chase" node, after the Move To, make a sequence node with our new node shoot and then a wait node (necessary otherwise the AI is going to shoot every frame). To this sub-sequence add an infinite loop decorator, so that it keeps going until the player goes out of AI's line of sight causing the "can see player?" decorator to fail (this happens because of the "observer aborts" property, if it wasn't for this option we would stay in the chase subsequence indefinitely).

At the moment, there is a bug: the AICharacter shoots, but it hits itself instead of the player! We'll solve this bug in lesson 203

## 202 - BTServices In C++

We're gonna introduce a new element of BTs, services: we're gonna find out what they are, how they help us with persistent running behavior and how we can write our own in C++.

There are only a few pieces of logic left in our control that aren't in our behavior tree:
- Setting the StartLocation at BeginPlay (but we like it in C++, we'll leave that here)
- Setting PlayerLocation
- Setting LastKnownPlayerLocation

Both of the latter can be set via something called a BTService. 

A service is essentially a way of running something every tick or every so often but only whenever a particular path is active. For example: service on chase node -> runs every tick while doing tasks under the chase sequence, if we are in investigate it doesn't.

At the moment the AI doesn't focus on the player: if we move around it, it doesn't rotate. There is a built-in sequence in Unreal called "Set Default Focus", by selecting PlayerLocation the focus will be set each tick on our player-controlled character if we're in the chase sequence.

We now want to implement a custom c++ service to update the LastKnownPlayerLocation and add it to the chase node.

New c++ class -> `BTService_BlackboardBase` because we want to know which key we'll be updating, let's call it `BTService_PlayerLocation`. `TickNode` is the method of `UBTService` we're interested in. This is the code:
```cpp
void UBTService_PlayerLocation::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) 
{
    Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
    
    APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
    if (!PlayerPawn)
        return;

    OwnerComp.GetBlackboardComponent()->SetValueAsVector(
        GetSelectedBlackboardKey(), 
        PlayerPawn->GetActorLocation()
    );
}
```

Why don't we need `if (LineOfSightTo(PlayerPawn))` in the service? Because we'll now make another service, `UBTService_PlayerLocationIfSeen`, which will update or clear the PlayerLocation depending if we have LineOfSight or not, and we'll add this service to the selector node that's immediately attached to the ROOT node. This will run every frame because it's on the top node and, in the moment we clear, the decorator on the chase node breaks it immediately due to "Observer aborts" set to "Both". Therefore the `UBTService_PlayerLocation::TickNode` above automatically doesn't run when there is no line of sight, we don't need the condition.

Here is the code for the `UBTService_PlayerLocationIfSeen::TickNode` (`UBTService_PlayerLocationIfSeen` has been created with the same base class as `UBTService_PlayerLocation`):
```cpp
void UBTService_PlayerLocationIfSeen::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) 
{
    Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

    APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
    if (!PlayerPawn)
        return;
    
    // AIController that owns the UBehaviourTreeComponent.
    AAIController* OwnerController = OwnerComp.GetAIOwner();
    if (!OwnerController)
        return;
    
    if(OwnerController->LineOfSightTo(PlayerPawn))
    {
        OwnerComp.GetBlackboardComponent()->SetValueAsVector(
            GetSelectedBlackboardKey(), 
            PlayerPawn->GetActorLocation()
        );
    }
    else
    {
        OwnerComp.GetBlackboardComponent()->ClearValue(
            GetSelectedBlackboardKey()
        );
    }
}
```

We just transferred all the functionality that was in `ShooterAIController::Tick` to the selector. What's the advantage? What we can now control everything from the BT, which is way simpler than changing code.

## 203 - Ignoring Actors In Line Traces

We are going to be fixing those bugs with ray tracing so that we can kill the character without it killing itself, and we can run over the spot where they've been by disabling their capsule collider.

#### Shooting itself

Gun.cpp -> LineTraceSingleByChannel is done starting at player viewpoint, which his around the eyes for the AI. This implies that we cross the gun owner's capsule component first. In fact we have the same issue with the player controlled character, but in its case the camera is behind the shoulder so we circumvent the capsule, but we could hit our capsule just as easily if our camera was in a different location.

How do we stop this? Go to `PullTrigger()`: `LineTraceSingleByChannel` has an argument of type `FCollisionResponseParams`. If we create an instance of this class, we can choose some actors to ignore via `AddIgnoredActor()` and then pass the instance to `LineTraceSingleByChannel`!
```cpp
FCollisionQueryParams Params;
Params.AddIgnoredActor(this); // Ignoring the gun
Params.AddIgnoredActor(GetOwner()); // Ignoring the ShooterCharacter that owns this gun
bool bHitSomething = GetWorld()->LineTraceSingleByChannel(
	Hit, 
	PlayerCameraLocation, 
	LineTraceEnd, 
	// Value the engine assigned to our custom channel
	ECollisionChannel::ECC_GameTraceChannel1, 
	Params
);
```

#### Disable capsule and shooting when dead

ShooterCharacter.cpp, `TakeDamage()` function. When the character is dead (we can check this via the `IsDead()` function we made a while ago) we need to do some cleanup:
- Detach the controller from the character. This will stop the AI to be able to shoot and move around in the world. 
- Disable the capsule component so that we can disable collisions.

```cpp
if (IsDead()) // Death cleanup
{
  // By detaching the controller the character can no longer move around nor shoot.
  DetachFromControllerPendingDestroy();
  // Disabling the capsule component we disable collisions with the dead character.
  GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}
```

## 204 - Ending The Game

the next thing is to actually set up some game end conditions so that we can display a nice message to our player saying "you've lost" or "you've won".

Classes  involved in ending the game:
- `ShooterGameMode`: decides rules about the game, like who won and who lost. We'll need to know who has been killed, so here we'll have a method called `PawnKilled` which will be called from our shooter pawn when it dies, whether or not it's the AI or the player that is doing the dying.
- We'll have different game modes, such as `KillEmAllGameMode`, derived from our main one, that override `PawnKilled` in order to decide differently on the rules of the game. Therefore, the original `PawnKilled` in `ShooterGameMode` will be virtual.
- The UI logic and restarting logic is something that should happen in the player controller. This is especially true in games that are multiplayer because the game mode doesn't actually exist on the players machine, it only exists on the server, so any UI stuff should be going on in the player controller. The root `PlayerController` class has a virtual method called `GameHasEnded()`, so our `ShooterPlayerController` will override it.

When it dies, `ShooterPawn` will call `PawnKilled()` on a `ShooterGameMode*` that will atually point to one of our game modes, like `KillEmAllGameMode`, executing the overridden version of that game mode. From there, we'll call `GameHasEnded()` on a `PlayerController*` that will actually point to our `ShooterPlayerController`, so that the overridden version that we wrote is executed instead.

Our root game mode will be the default one created by the C++ project, in our case `SimpleShooterGameModeBase`

1. Create the `PawnKilled()` public virtual method

2. Create a new c++ class using `SimpleShooterGameModeBase` as base, called `KillEmAllGameMode`
3. Go to `ShooterCharacter::TakeDamage()`, inside the `if(IsDead())` and call `PawnKilled()` from there
```cpp
ASimpleShooterGameModeBase* GameMode = GetWorld()->
	GetAuthGameMode<ASimpleShooterGameModeBase>();
if(GameMode) // Warning our GameMode that this has been killed
	GameMode->PawnKilled(this);	
```
4. Override `PawnKilled()` in `KillEmAllGameMode` and make it log something for testing
5. Derive a BP class based on  `KillEmAllGameMode`, `BP_KillEmAllGameMode`
6. Set its default pawn class to `BP_ShooterCharater`
7. Set `BP_KillEmAllGameMode` as default game mode in Project settings > Maps & Modes

## 205 - Setting Timers In C++

We are going to be putting together a player controller that will receive information that the game has ended from the game mode and will create this restarting behavior that after five seconds reloads the level after the player has died.

1. First thing `PawnKilled()` should do is check if the game is over (i.e. if the player won or lost) and then call `GameHasEnded()` on the controller if that's the case. This can be done as follows in case of loss:
```cpp
void AKillEmAllGameMode::PawnKilled(APawn* PawnKilled) 
{
    Super::PawnKilled(PawnKilled);
    // UE_LOG(LogTemp, Warning, TEXT("%s died"), *PawnKilled->GetName());
    
    APlayerController* PlayerController = Cast<APlayerController>(
        PawnKilled->GetController()
    );
    if(PlayerController) // Player died
        // We lost -> 2nd argument is false
        PlayerController->GameHasEnded(nullptr, false);
}
```

2. We don't have a custom player controller! We want one so that we can make a custom override of `GameHasEnded()`! Create a c++ class that is based on `PlayerController`, called `ShooterPlayerController` then create a BP derivate, go to the `KillEmAllGameMode` and set the BP derivate as default player controller class.
3. Override `GameHasEnded()` in `ShooterPlayerController` and make it log something for test purposes.
> BUG: you'll notice that even if the code compile you don't log anything. That's because , in `TakeDamage()`, we use `DetachFromControllerPendingDestroy()` before calling  `PawnKilled()`! Fix that and you'll be fine

4. We now want to restart the game on a timer inside our `GameHasEnded()` override. `APlayerController` class has a `RestartLevel()` method, we will delegate it to our timer!
```cpp
void AShooterPlayerController::GameHasEnded(class AActor* EndGameFocus, bool bIsWinner) 
{
    Super::GameHasEnded(EndGameFocus, bIsWinner);
    // UE_LOG(LogTemp, Warning, TEXT("GAME HAS ENDED."));
    GetWorldTimerManager().SetTimer(
        RestartTimer, // FTimerHandler
        this, 
        &APlayerController::RestartLevel, // Function to call (delegate)
        RestartDelay // After how much time the function should be called
    );
}
```
Because of the `if(PlayerController)` check in `PawnKilled`, this `GameHasEnded` executes only if the player dies, not if the AI dies.

## 206 - Displaying A Lose Screen

We are going to be displaying a lose screen and we're gonna be doing it entirely from within C++ to spawn that widget.

Add new -> user interface -> widget blueprint. 90% of the time you don't need a c++ base class for UI stuff, unless your widget is super complicate. It's not our case, so we're making the widget purely through blueprints, and it will be called `WBP_LoseScreen`.

Now that we made the widget via blueprint, it's now time to spawn it, and we'll handle the spawn via C++. First of al: where are we spawning the widget from? The PlayerController is a good place, let's do this in `GameHasEnded`. To make the PlayerController aware of the class we want to spawn we need in the header:
```cpp
UPROPERTY(EditAnywhere)
TSubclassOf<UUserWidget> LoseScreenClass;
```
Thanks to `TSubclassOf` we can restrict the number of classes to choose from in the editor. In the code above, `UUserWidget` needs the `class UUserWidget` statement at the top to be recognized.

Ok, now we need to spawn this: we need to use a function called `CreateWidget()`, that needs an include to be used:
```cpp
// 1st param is the owner. Only certain classes can own widgets, and
// APlayerController is one of them.
// The 2nd param is set from the editor and it's our WBP_LoseScreen class
UUserWidget* LoseScreen = CreateWidget(this, LoseScreenClass);
```
Now we need to make the widget visible, at the moment it's not. To do so, we need to add it to the viewport via:
```cpp
if (LoseScreen)
  LoseScreen->AddToViewport();
```
Before compiling, go to the SimpleShooter.Build.cs file and in `PublicDependencyModuleNames.AddRange` add "UMG", otherwise you are going to get a shit tone of dll errors (these errors usually pop up when you're missing a module dependency).

Don't forget to select your "WBP_LoseScreen" class from the `BP_ShooterPlayerControlle`, otherwise you won't see the widget!

## 207 - Iterating Over Actors

We should also be notifying the AIControllers that the game has ended. How do we achieve this?

Let's create a private function in `KillEmAllGameMode` called `EndGame()`. In this function we'll iterate through all the controllers in our scene so that we can call `GameHasEnded()` on those controllers. To do so, we'll use an engine helper (include "EngineUtils.h")
```cpp
for(AController* Controller : TActorRange<AController>(GetWorld()))
{
    
}
```
`TActorRange<AController>(GetWorld())` returns us a collection of all the controllers in our world (aka the level). The content of the for then will be:
```cpp
if(Controller->IsPlayerController())
    Controller->GameHasEnded(nullptr, bIsPlayerWinner);
else
    Controller->GameHasEnded(nullptr, !bIsPlayerWinner);
```
Or, in a more compact way:
```cpp
bool bIsWinner = Controller->IsPlayerController() == bIsPlayerWinner;
Controller->GameHasEnded(Controller->GetPawn(), bIsWinner);
```
Now of course we have to change `PawnKilled()` to call this `EndGame()` function with the right value based on the dead pawn being the player or not.

Note that we substituted the nullptr in `GameHasended` because if the player has actually won it's gonna try and take away the focus from our PlayerPawn. With `Controller->GetPawn()` the camera will stil be pointing on your pawn. Why is there an option to detach the camera from the PlayerPawn when the game ends? In some multiplayer games, when the player dies the camera will start following the enemy that killed us, and the way to do so is pass in the enemy pawn

## 208 - Calculating The Win Condition

How can we calculate if the player has won?
1. `PawnKilled()` -> if the controller is not the player's we subtract one from the count of our enemies: if the count reaches zero the player wins. This requires us to put how many enemies there are in the world in two places essentially: in the game mode and in the world, and those two can get out of sync. In this sense, it's a bit weak.
2. Re-count all the enemies that are in the world every time a pawn that isn't the player is killed: if anyone is still alive we're not going to end the game yet, otherwise the player wins. 
   
Let's go for number 2: to do this, we need a way to query the AIControler to check whether is dead or not from PawnKilled. Let's create this function:
```cpp
bool AShooterAIController::IsDead() const
{
    AShooterCharacter* ControlledCharacter = Cast<AShooterCharacter>(GetPawn());
    if (ControlledCharacter)
        return ControlledCharacter->IsDead();
    
    // The check above could fail because AIController detaches from pawn when it dies
    // see AShooterCharacter::TakeDamage()
    return true;
}
```
Technically you don't need the function above: from `PawnKilled` you could just use `GetPawn` on the `AShooterAIController`, cast it to `AShooterCharacter` and then call the `IsDead` function of this class. However, that's generally not best practice because it means that if suddenly something changes about how we calculate death and or the AIController is going to control different types of pawn, suddenly we need to go and change the game mode, which doesn't make sense.

Ok, at this point we cycle through all the AShooterAIController in the level to check whether the player won or not:
```cpp
// Inside PawnKilled()
for (AShooterAIController* AIController : TActorRange<AShooterAIController>(GetWorld()))
{
    // If one of the AIControllers isn't dead we quit PawnKilled() without calling EndGame()
    if (!AIController->IsDead()) 
        return;
}
EndGame(true);
```
Finally, in `GameHasEnded`, we need to call a different widget if the player has won. The logic is very simple and you just need to create another widget for the victory screen.

## 210 - Weapon Sound Effects

We'll have a muzzle sound and an impact sound. These are gun sounds obviously, so the places where we're already spawning emitters are where we'll also add the sound.

UGameplayStatics::PlaySoundAtLocation is one of the function we'll use: it will play the sound unattached to any object, so that when we'll move around it'll just stay fixed in the location until it's finished.  
But we also want to play an attached sound: how do we do that?

PlaySoundAtLocation has a USoundBase argument. Why is it a SoundBase and not just a SoundWave? If we go to our assets folder, we can see that all of the sound assets are of type USoundWave, a class that  inherits from USoundBase! This means that PlaySoundAtLocation doesn't work only with SoundWave objects.

Which functions in GameplayStatics.h  take a SoundBase? One of them is `SpawnSoundAttached`, which is exactly what we're looking for!

For both functions:
1. Create the UPROPERTY(EditeAnywhere) for the USoundBase* in the Gun header
2. Call them close to their particle counterparts. The function calls are essentially the same
3. Compile and go to the gun blueprint to add some sound effects

## 211 - Randomized Sound Cues

We're going to randomize the sound made by our gun and projectiles using a feature in Unreal 4 called SoundCues.

SoundCues are a new type of asset. Name convention: `A_<YourCueName>_C`
- A: stands for "Audio Asset"
- C: stands for "Sound Cue", which is a type of audio asset

They have an editor. With audio cue you can do things such as play different sounds depending on the material, add some looping, crossfade etc... we're interested in randomization.

wave player nodes (each of them collected to a different sound) + random node + output of the sound cue = randomization.

 Randomize without replacement: you can't play the same sound again wif you haven't played all the others first.
 
 More randomization! Modulator node randomizes volume and pitch
 
 After making 2 sound cues, one for rifle shot and one for bullet impact, we can go to the BP_Gun and select those as Muzzle and Impact sounds, because Sound Cues are of type SoundBase!!
 
 ## 212 - Sound Spatialization
 
 We're going to be adding attenuation to our sounds so that, depending on our distance from sounds, they are going to be louder or quieter. We're also going to add spatialization so that the audio is going to be panning from our left and right speakers and we can hear whether the bullets are coming from our left or our right.
 
 Spatialization: depending on where the sound sources are relative to where you're looking, one of the speakers / headphones will play more audio than the other.
 
 Cue editor -> scroll down on the details panel until you dfind the attenuation section -> tick "override attenuation" which unlocks a bunch of settings.
 
 But we can also create an attenuation asset, which is more scalable because we can apply it to multiple sound cues. Untick "override attenuation", go to attenuation settings and create a new one called `ATT_Default`
 - Attenuation distance
   - Attenuation function
   - Attenuation shape
   - Inner Radius: max distance from the source where we can get max volume
   - Falloff Distance: beyond this distance you get 0 volume
- Spatialization distance
  - Spatialization method: panning means "if the source is on my left play more of the sound from my left speaker". Binaural (the other option) also gives you an sense of up and down, but only works with headphones
  - Stereo spread: ho w far apart things are going to sound

Final section of this lecture is on how to debug sounds, useful but I won't report it here

## 213 - Crosshairs and HUDs

We'd like to have some UI in our game to know how much health we've left and to make aiming easier. How do we do that? With Widgets! Let's make a `WPB_HUD` widget to handle this stuff

If you have a crosshair graphic, you could use the image component of the canvas to make the crosshair, but we're just gonna use a textbox with the + character 😆 

Once you created the new WBP, go to `ShooterPlayerController` class and:
1. Override `BeginPlay`
2. Add the configuration to the header file (via `TSubclassOf`)
3. Add the widget to the viewport from `BeginPlay`
4. compile and select `WPB_HUD` as class from the `BP_ShooterPlayerController`

The crosshair should disappear when the game ends. To do so, make a private ptr to the UUserWidget so that you can call `RemoveFromViewport()` on it in `GameHasEnded()`.
 
 UI stuff should always be done in PlayerController derived classes like we're doing!!
 
 ## 214 - Health Bars
 
 `WBP_HUD` widget -> palette -> progress bar. Percentage: percentage of 1 is full health, percentage of 0 is death.
 
 Once you aded the new element to the widget, we need to link it to the gameplay. To do that, we need to expose a function from our ShooterCharacter class to BP so that we can get this in the widget: make a blueprint-pure function called `GetHealthPercent` that returns a float.
 
 Now go to the WBP, progressbar->percentage->Bind->add bind. This will create a function that updates the percentage value of the bar (by default the function is called Get Percentage 0, you can rename it). Use `Get Owning Player Pawn` node to get the pawn and then cast it to ShooterCharacter with the apposite node. On its output, use `Get Health Percent`, the function we created before, and use it as return value.
 
 To differentiate the player from the AI, we're going to create a child class of the BP_ShooterCharacter, BP_PlayerCharacter, which will have a different Health and mesh. Then, from the blueprint of the KillEmAllGameMode, set the  BP_PlayerCharacter as default pawn class.
 
 ## 215 - Aim Offsets
 
 At the moment, if we aim up and down our character's arms aren't doing anything and the gun keeps pointing in the same direction (even though the shot is effectively in the direction we're looking).  
 In this lecture we'll finally be able to make our mesh look up and down while we aim up and down.
 
 Animation blueprint (ABP_SHooterCharacter) -> new type of animation asset: animation offset. Top right buttons -> animation arrow -> `Idle_AO_Combat`, which is a aim offset (underlined in blue).
 
 By opening the asset, you get an editor very similar to the blend space one, except that moving the green diamond moves the mesh. This is known as an additive animation (you can see that from the pane on the left that says "Additive Settings"). This means that this animation can be added on top of another animation, like adding the info about where my arms have moved relative to the run pose and att them on top.
 
 So, where do we want to add this information in the event graph? After the blendspace, so that it goes on top of the BS animation!
 
 Right click -> Idle_AO_Combat. Notice that it has an input animation since it is an additive animation (the BS doesn't because it's not additive). Promote the Pitch / Yaw pins to variables. Now we need to get them from the game, and for that we need to go to the event graph.
  
The `Get Control Rotation` node can give us the pitch and the yaw from the `Try Get Pawn Owner` node. Problem: `Get Control Rotation` is entirely global, it doesn't take into account the direction our player is already facing. To take that into account: `Get Actor Rotation` and then we want to find the rotation between the actor rotation and the control rotation. You can do that via the `Delta (Rotator)` node.

One last problem: this doesn't work with AI controlled characters, they shoot up and down without moving their arms! Why?

Go to our AI's behavior tree -> set default focus selector -> we're setting our default focus to a location! By default, if you set focus to location, unreal is not going to update your control rotation, it's not going to do the pitch.

Go to the BB: change PlayerLocation key name to Player and its type to Object (base class Actor). Now things change in the BT: instead of moving to the player location we just move to towards the player actor. Change the `Update Player Location If Seen` selector from c++ so that it sets the actual actor by using `SetValueAsObject` instead.

## 216 - Animation State Machines

At the moment our walking/running animation keeps going even when our character jump (airborne), which doesn't make any sense.

State machine are a new part of the animation BP, which allow us to transition in a jumping state and back down to grounded by switching the value of a boolean

AnimGraph -> right click -> `Add New State Machine` node. Now the bottom left tab in the editor shows the state machine under our AnimGraph. Name the state machine Locomotion.

Locomotion editor -> right click -> add state. Dragging from one state to the other will create a so-called state transition. If you double click on a state you are taken to an output pin and that can be used in the same way the AnimGraph is used: to build up an animation pose. That is the animation that will be played when the state machine is in that state.

How do we entry a given state?
1. Because are entry node points to it
2. Via a series of transition.

Each transition has properties, by double clicking we have another editor pop up and we can have some boolean property.

Our case: we'll have two states, grounded and airborne. In the grounded state we'll play the ground locomotion, i.e. the BS.

Cut the BS and paste it in the Grounded state, attach the state machine to the `Idel_AO_Combat` node.

Airborne state -> Jump_apex_Combat animation directly attached to the output with Loop Animation option disabled.

Attach the two states togheter, making a transition, and create a boolean variable `IsAriborne` that toggles it (simply attach it to the result node)! You'll need another transition to go back to the Grounded state with the boolean variable negated (NOT node).

## 217 - Complex State Machines

We now need to hook this new variable we made to gameplay. The only waty to do this it through the Event Graph of the ABP_ShooterCharacter.

`Try Get Pawn Onwer` -> `Cast To Character` -> `As Character` pin -> `Is Falling` -> `SET IsAirborne`

Ok, it works. Let's complicate things a bit: we want to play a jumping animation, an nimation where we're airborne, and then a landing animation when we hit the ground. We need more states in our state machine!

once you created the jumping state, put:
- grounded jumping transition if `Is Airborne` 
- jumping to airborne transition. When is it going to happen? When we finished the jumping animation! To transition between the jumping anim to the airborne anim, select the jumping to airborne transition and check the `Automatic Rule Based on Sequence Player in State` in the details panel. This way, when the previous state ends (jumping), we immediately enter the next one (airborne) without needing a condition. Via Blend settings you can set the duration of the blend

Create a landing state, transition from Airborne to Landing on bool and from Landing to Grounded when finished. Transition mode: how the fraction of landing over grounded anim changes over time.

In the final minutes of the lecture, as challenge, he shows you how to put the entire anim graph into a single state machine `Health Status` with an Alive state which plays the `Locomotion` state machine and the `Idel_AO_Combat` node, and a Dead state which just plays the death animation. In other words: you can put a state machine inside another, cool!

## 218 - Final Touches

#### Background Music

Main editor, place actors tab, ambient sound. Drag it into the worl and select a music from the details panel

#### Introduction Sound

BP_PlayerCharacter, full editor, event graph, BeginPlay node, play sound at location node.